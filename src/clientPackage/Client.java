package clientPackage;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import clientPackage.MessageReceiver;
import clientPackage.MessageWriter;
/**
 * Client.
 * 
 * <p>
 * Classe {@code Client} est en de la demande de la connexion, Lorsqu'une
 * demande de connexion est acceptée, elle lance 2 threads coutant le message
 * envoyé et l'entre standard.
 * 
 * @author Xianhua ZHOU
 * @author Zilu ZHENG
 * @author Thomas LERNER
 * @version 1.0
 */
public class Client {
	protected static int aliveFlag=0;
	/**
	 * Demande la connexion, si le pseudo saisi est libre, lance le thraed
	 * {@code MessageWriter} et le thread {@code MessageReceiver}
	 * 
	 * @param args Non utilis.
	 * @throws IOException Exception durant l'écriture ou la lecture
	 * @return void
	 */
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			Socket client = new Socket("localhost", 10081);
			DataOutputStream outs = new DataOutputStream(client.getOutputStream());
			DataInputStream ins = new DataInputStream(client.getInputStream());

			String pseudoToSend = "";
			String msgReceived = "";

			// Saisit un pseudo en vérifiant la disponibilité
			do {
				System.out.println(ins.readUTF());
				pseudoToSend = sc.nextLine();
				outs.writeUTF(pseudoToSend);
				msgReceived = ins.readUTF();
				// réponse avec "pseudo déjà utilisé" ou "Réussi"
				if (msgReceived.equals("Réussi"))
					break;
				else
					System.out.println(msgReceived);
			} while (true);

			// Lancer le thread de la lecture/l'écriture
			Thread msgWriter = new Thread(new MessageWriter(client));
			Thread msgReceiver = new Thread(new MessageReceiver(client, pseudoToSend));
			msgWriter.start();
			msgReceiver.start();
			aliveFlag=1;
		} catch (IOException ex) {
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
