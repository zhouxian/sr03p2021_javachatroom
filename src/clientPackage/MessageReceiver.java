package clientPackage;

import java.io.*;
import java.net.*;
import java.util.logging.Logger;
import java.util.logging.*;
import clientPackage.Client;
/**
 * Thread en charge de la lecture du message envoy¨¦.
 * 
 * <p>
 * Classe {@code MessageReceiver} hérite de la classe {@link Thread}, démarré
 * par la classe {@link Client}. Elle est en charge de la réception de message
 * depuis le serveur, et l'affiche à la sortie standard.
 * 
 * @author Xianhua ZHOU
 * @author Zilu ZHENG
 * @author Thomas LERNER
 * @version 1.0
 */
public class MessageReceiver extends Client implements Runnable{

	private Socket client;
	private DataInputStream ins;
	private String pseudo;

	/**
	 * Constructeur
	 * 
	 * @param client Un objet {@code Socket}.
	 * @param pseudo Pseudo du client.
	 * @return void
	 */
	public MessageReceiver(Socket client, String pseudo) {
		this.client = client;
		this.pseudo = pseudo;
	}

	/**
	 * Boucler pour lire le message du serveur jusqu'au départ du client et fermer
	 * la {@code Socket}
	 * 
	 * @throws IOException durant l'écriture ou la lecture
	 * @return void
	 */
	@Override
	public void run() {
		try {
			ins = new DataInputStream(client.getInputStream());
			String msgReceived = "";

			while (!client.isClosed()) {
				msgReceived = ins.readUTF();
				if (aliveFlag==2) {
					client.close();
					break;
				}
				System.out.println(msgReceived);
				/*
				 * Lorsque l'utilisateur se connecte, une ligne pointillée est imprimée sur la
				 * sortie standard locale.
				 */
				if (msgReceived.matches(pseudo + " a rejoint.*"))
					System.out.println("-----------------------");
			}
		} catch (IOException ex) {
			Logger.getLogger(MessageReceiver.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
