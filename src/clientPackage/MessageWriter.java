package clientPackage;

import java.util.Scanner;
import java.io.*;
import java.net.*;
import java.util.logging.Logger;
import java.util.logging.*;
import clientPackage.Client;
/**
 * Thread en charge de la lecture des entrées standard et l'envoi du message.
 * 
 * <p>
 * Classe {@code MessageWriter} hérite de la classe {@link Thread}, démarré par
 * la classe {@link Client}. Elle lit le message depuis l'entrée standard et
 * l'envoie au serveur.
 * 
 * @author Xianhua ZHOU
 * @author Zilu ZHENG
 * @author Thomas LERNER
 * @version 1.0
 */
public class MessageWriter extends Client implements Runnable{

	private Socket client;
	private DataOutputStream outs;

	/**
	 * Constructeur
	 * 
	 * @param client Un objet {@code Socket}.
	 * @return void
	 */
	public MessageWriter(Socket client) {
		this.client = client;
	}

	/**
	 * Boucler pour lire le message de l'entrée standard, jusqu'au départ du client.
	 * 
	 * @throws IOException durant l'écriture ou la lecture
	 * @return void
	 */
	@Override
	public void run() {
		try {
			Scanner sc = new Scanner(System.in);
			outs = new DataOutputStream(client.getOutputStream());
			String msgToSend = "";
			do {
				msgToSend = sc.nextLine();
				outs.writeUTF(msgToSend);
				if (msgToSend.equals("exit"))
					break;
			} while (true);
			System.out.println("Au revoir!");
			sc.close();
			aliveFlag  = 2;
		} catch (Exception ex) {
			Logger.getLogger(MessageWriter.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
