package serverPackage;

import java.io.*;
import java.net.*;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Chatroom serveur.
 * 
 * <p>
 * Classe {@code Serveur} est le père de la classe ClientManager, elle est en
 * charge de la création de connexion {@code Socket}. Lorsqu'une demande de
 * connexion arrive, le serveur stocke l'objet {@code Socket} connecté dans un
 * tableau, et lance un thread écoutant et diffusant le message envoyé.
 * 
 * @author Xianhua ZHOU
 * @author Zilu ZHENG
 * @author Thomas LERNER
 * @version 1.0
 */
public class Serveur {

	protected static Hashtable<String, Socket> commsTable = new Hashtable<String, Socket>();

	/**
	 * Création et établissement de la connexion Socket.
	 * 
	 * @param args Non utilisé.
	 * @throws IOException Exception durant l'écriture ou la lecture
	 * @return void
	 */
	public static void main(String[] args) {
		try {
			ServerSocket conn = new ServerSocket(10081);
			boolean err = false;

			do {
				try {
					Socket comm = conn.accept();
					DataOutputStream outs = new DataOutputStream(comm.getOutputStream());
					DataInputStream ins = new DataInputStream(comm.getInputStream());
					// Boucle infinie (s'il n'y pas d'erreur) pour accepter les demandes entrantes
					do {
						outs.writeUTF("Entrez votre pseudo :");
						String pseudo = ins.readUTF();
						// Stocke l'objet Socket si le pseudo est libre
						synchronized (commsTable) {
							if (commsTable.containsKey(pseudo)) {
								outs.writeUTF("Pseudo déjà utilisé !");
							} else {
								outs.writeUTF("Réussi");
								commsTable.put(pseudo, comm);
								Thread thread = new Thread(new ClientsManager(comm, pseudo));
								thread.start();
								break;
							}
						}
					} while (true);
				} catch (IOException e) {
					err = true;
					Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, e);
				}
			} while (!err);

			conn.close();
		} catch (IOException ex) {
			Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
