package serverPackage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread en charge de la réception et la diffusion du message d'un client.
 * 
 * <p>
 * Classe {@code ClientsManager} hérite de la classe {@link Serveur}, implémenté
 * l'interface {@link Runnable}. Elle joue à un {@link Thread} , démarré par la
 * classe {@link Serveur}.
 * 
 * 
 * @author Xianhua ZHOU
 * @author Zilu ZHENG
 * @author Thomas LERNER
 * @version 1.0
 */
public class ClientsManager extends Serveur implements Runnable {

	private Socket comm;
	private String pseudo;
	private DataInputStream ins;

	/**
	 * Constructeur
	 * 
	 * @param comm   Un objet {@code Socket}.
	 * @param pseudo Pseudo du client
	 */
	public ClientsManager(Socket comm, String pseudo) {
		this.comm = comm;
		this.pseudo = pseudo;
	}

	/**
	 * Ecoutant et diffusant le message de toutes les sockets stockées dans le
	 * tableau.
	 * 
	 * @throws IOException durant l'écriture ou la lecture
	 */
	@Override
	public void run() {
		try {
			ins = new DataInputStream(comm.getInputStream());
			broadcastMsg(pseudo + " a rejoint la conversation");
			while (true) {
				String msgReceived = receiveMsg();
				if (msgReceived.equals("exit")) {
					closeConnection();
					break;
				}
				broadcastMsg(pseudo + " a dit: " + msgReceived);
			}

		} catch (IOException ex) {
			try {
				closeConnection();
			} catch (IOException e) {
				Logger.getLogger(ClientsManager.class.getName()).log(Level.SEVERE, null, e);
			}
		}
	}

	/**
	 * Diffuser le message à toutes les sockets stockées dans le tableau.
	 * 
	 * @param msg Message à diffuser.
	 * @throws IOException durant l'écriture ou la lecture
	 */
	private void broadcastMsg(String msg) throws IOException {
		synchronized (commsTable) {
			for (Socket s : commsTable.values()) {
				if (s.isClosed()) {
					System.out.println("out");
					continue;
				}
				DataOutputStream socketOuts = new DataOutputStream(s.getOutputStream());
				socketOuts.writeUTF(msg);
			}
		}
	}

	/**
	 * Lire le message depuis client.
	 * 
	 * @return {@code String} le message reçu.
	 * @throws IOException
	 */
	private String receiveMsg() throws IOException {
		return ins.readUTF();
	}

	/**
	 * Retirer la socket depuis le tableau et la fermer.
	 * 
	 * @throws IOException
	 */
	private void closeConnection() throws IOException {
		broadcastMsg("L'utilisateur " + pseudo + " a quitté la conversation");
		// enlever la socket de commsTable
		synchronized (commsTable) {
			commsTable.remove(pseudo);
		}

		comm.close();
	}
}
